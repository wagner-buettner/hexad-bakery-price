package com.hexad.bakery.service;

import com.hexad.bakery.exception.ProductException;
import com.hexad.bakery.model.Product;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import static com.hexad.bakery.common.Constants.INVALID_PRODUCT_CODE;

public class ProductServiceTest {

    private ProductService productService;

    @Before
    public void setUp() {
        productService = ProductService.getInstance();
    }

    @Test
    public void testIfIsValid_FindOneProductVS5() {
        Product productVS5 = productService.findProduct("VS5");
        Assert.assertEquals("VS5-Vegemite Scroll", productVS5.toString());
        Assert.assertEquals(6.97f, productVS5.getPrice(3), 0.02);
        Assert.assertEquals(8.97f, productVS5.getPrice(5), 0.02);
    }

    @Test
    public void testException_FindInvalidProduct() throws Exception {
        try {
            productService.findProduct("W-A-G");
        } catch (ProductException pe) {
            Assert.assertEquals( INVALID_PRODUCT_CODE + "W-A-G", pe.getMessage() );
        }
    }

}

package com.hexad.bakery.service;

import org.junit.Assert;
import org.junit.Test;

import static com.hexad.bakery.common.Constants.INVALID_ORDER;
import static com.hexad.bakery.common.Constants.INVALID_PRODUCT_CODE;
import static org.junit.Assert.assertEquals;

public class OrderServiceTest {
    OrderService orderService = new OrderService();

    @Test
    public void testThreeValidOrder() {
        String packageBreakdown;

        packageBreakdown = orderService.processOneOrder("23 VS5");
        assertEquals("23 VS5-Vegemite Scroll $42.949997\n" +
                              "\t4 X 5 $8.99\n" +
                              "\t1 X 3 $6.99", packageBreakdown);

        packageBreakdown = orderService.processOneOrder("31 MB11");
        assertEquals("31 MB11-Blueberry Muffin $101.75\n" +
                              "\t3 X 8 $24.95\n" +
                              "\t1 X 5 $16.95\n" +
                              "\t1 X 2 $9.95", packageBreakdown);

        packageBreakdown = orderService.processOneOrder("26 CF");
        assertEquals("26 CF-Croissant $49.88\n" +
                              "\t2 X 9 $16.99\n" +
                              "\t1 X 5 $9.95\n" +
                              "\t1 X 3 $5.95", packageBreakdown);
    }

    @Test
    public void testInvalidOrderFormat() {
        String output = orderService.processOneOrder("VS5 23");
        Assert.assertEquals(INVALID_ORDER + "VS5 23", output);
    }

    @Test
    public void testInvalidProduct() {
        String output = orderService.processOneOrder("15 VS10");
        Assert.assertEquals(INVALID_PRODUCT_CODE + "VS10", output);
    }
}

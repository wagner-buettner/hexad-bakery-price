package com.hexad.bakery.exception;

public class OrderException extends RuntimeException {
    public OrderException(String msg) {
        super(msg);
    }
}

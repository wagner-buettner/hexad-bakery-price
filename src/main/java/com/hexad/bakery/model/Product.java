package com.hexad.bakery.model;

import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static java.util.stream.Collectors.toList;

public class Product {
    private String code;
    private String name;
    private Map<Integer, Float> packPriceMap;

    public Product(String code, String name) {
        this.code = code;
        this.name = name;
        this.packPriceMap = new HashMap<>();
    }

    public String getCode() {
        return code;
    }

    public void addPackPrice(Integer packSize, Float price) {
        this.packPriceMap.put(packSize, price);
    }

    public Float getPrice(Integer packSize) {
        return packPriceMap.get(packSize);
    }

    public List<Integer> getSortedPackList() {
        return packPriceMap.keySet().stream().sorted(Comparator.reverseOrder()).collect(toList());
    }

    @Override
    public String toString() {
        return code + "-" + name;
    }
}

package com.hexad.bakery.model;

public class Order {
    private String productCode;
    private Integer quantity;

    public Order(String productCode, Integer quantity) {
        this.productCode = productCode;
        this.quantity = quantity;
    }

    public String getProductCode() {
        return productCode;
    }

    public Integer getQuantity() {
        return quantity;
    }

    @Override
    public String toString() {
        return productCode + "-" + quantity;
    }
}

package com.hexad.bakery.common;

import java.io.*;
import java.util.List;
import java.util.stream.Collectors;

public class FileUtils {

    public static List<String> loadTextFile(String fileName) {
        BufferedReader bufferedReader = null;

        try {
            InputStream inputStream = new FileInputStream(fileName);
            bufferedReader = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"));

            return bufferedReader.lines().skip(1).collect(Collectors.toList());
        } catch (Exception e) {
            throw new RuntimeException("Error opening file: " + fileName);
        } finally {
            try {
                if (bufferedReader != null) {
                    bufferedReader.close();
                }
            } catch (IOException e) { }
        }
    }
}

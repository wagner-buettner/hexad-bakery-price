package com.hexad.bakery.common;

public class Constants {
    public static final String ORDERS_FILE = "orders.txt";
    public static final String PRODUCT_FILE = "product.csv";
    public static final String PRICE_FILE = "product_price.csv";
    public static final String INVALID_ORDER = "Invalid order: ";
    public static final String INVALID_PRODUCT_CODE = "Invalid Product Code: ";
}

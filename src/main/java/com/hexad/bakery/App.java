package com.hexad.bakery;

import com.hexad.bakery.service.OrderService;

import static com.hexad.bakery.common.Constants.ORDERS_FILE;

import java.io.IOException;

public class App
{
    public static void main( String[] args ) throws IOException {
        try {
            OrderService orderService = new OrderService();
            orderService.process(ORDERS_FILE)
            .forEach(App::write);

        } catch (IOException ex) {
            System.out.println(ex.toString());
            throw new IOException("Error reading orders file" + ORDERS_FILE);
        }

    }

    public static void write(String value) {
        System.out.println(value);
    }
}

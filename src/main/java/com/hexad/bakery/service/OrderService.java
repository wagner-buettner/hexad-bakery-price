package com.hexad.bakery.service;

import com.hexad.bakery.exception.OrderException;
import com.hexad.bakery.exception.ProductException;
import com.hexad.bakery.model.Order;
import com.hexad.bakery.model.Product;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static com.hexad.bakery.common.Constants.INVALID_ORDER;
import static java.util.stream.Collectors.toList;

public class OrderService {
    private ProductService productService = ProductService.getInstance();
    private Order order;

    public List<String> process(String orderFile) throws IOException {
        try {
            return Files.lines(Paths.get(orderFile))
            .map(this::processOneOrder)
            .collect(Collectors.toList());
        } catch (IOException e) {
            e.printStackTrace();
            throw new IOException("Error reading file: " + orderFile);
        }
    }

    protected String processOneOrder(String orderToProcess) {
        try {
            order = parseOrder(orderToProcess);
            final Product product = productService.findProduct(order.getProductCode());
            final Integer quantity = order.getQuantity();

            return printBill(packageBreakdown(product, quantity), product, quantity);
        } catch (OrderException oe) {
            return oe.getMessage();
        } catch (ProductException pe) {
            return pe.getMessage();
        }
    }

    private Order parseOrder(String orderToSplit) {
        try {
            Order order = new Order( orderToSplit.split(" ")[1].trim()
                                   , Integer.parseInt(orderToSplit.split(" ")[0].trim()));
            return order;
        } catch (Exception ex) {
            throw new OrderException(INVALID_ORDER + orderToSplit);
        }
    }

    private Map<Integer, Integer> packageBreakdown(Product product, Integer quantity) {
        Map<Integer, Integer> packageBreakdown = new HashMap<>();

        List<Integer> packSizeList = product.getSortedPackList();

        int quantityOrder = quantity;
        int packListIndex = 0;
        int packSize = 0;

        while (quantityOrder > 0 && packListIndex < packSizeList.size()) {
            if(packSize > 0) {
                if(packSizeList.indexOf(packSize)+1 == packSizeList.size()) {
                    packSize = packSizeList.get(0);
                }

                if(packageBreakdown.containsKey(packSize)) {
                    quantityOrder += packSize;

                    if (packageBreakdown.get(packSize) > 1) {
                        packageBreakdown.put(packSize, packageBreakdown.get(packSize) - 1);
                    } else {
                        packageBreakdown.remove(packSize);
                    }

                    packListIndex = packSizeList.indexOf(packSize) + 1;
                }
            }

            for (int i=packListIndex; i<packSizeList.size(); i++) {
                if (quantityOrder/packSizeList.get(i) > 0) {
                    packSize = packSizeList.get(i);
                    packageBreakdown.put(packSize, quantityOrder/packSize);
                    quantityOrder = quantityOrder % packSize;
                }
            }

            packListIndex++;
        }

        return packageBreakdown;
    }

    private String printBill(Map<Integer, Integer> packageBreakdown, Product product, Integer quantity) {
        StringBuilder stringBuilder = new StringBuilder();
        float totalOrder = 0f;

        List<Integer> packageBreakdownReverse = packageBreakdown.keySet()
                                                                .stream()
                                                                .sorted(Comparator.reverseOrder())
                                                                .collect(toList());

        for(Integer packSize :  packageBreakdownReverse) {
            totalOrder += packageBreakdown.get(packSize) * product.getPrice(packSize);
            stringBuilder.append("\n\t" + packageBreakdown.get(packSize)
                                        + " X "
                                        + packSize
                                        + " $"
                                        + product.getPrice(packSize));
        }

        return quantity + " " + product + " $" + totalOrder + stringBuilder.toString();
    }

}

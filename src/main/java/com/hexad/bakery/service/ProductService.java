package com.hexad.bakery.service;

import com.hexad.bakery.common.FileUtils;
import com.hexad.bakery.exception.ProductException;
import com.hexad.bakery.model.Product;
import com.hexad.bakery.model.ProductPrice;

import java.util.List;
import java.util.Map;

import static com.hexad.bakery.common.Constants.*;
import static java.lang.Float.parseFloat;
import static java.lang.Integer.parseInt;
import static java.util.Collections.emptyMap;
import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.toMap;
import static java.util.Objects.isNull;

public class ProductService {
    private static ProductService productService;
    private static Map<String, Product> productMap;

    private ProductService() {
        loadProductMap();
        loadPriceInProductMap();
    }

    public static ProductService getInstance() {
        return productService = new ProductService();
    }

    public Product findProduct(String productCode) {
        Product product = productMap.get(productCode);
        if (isNull(product)){
            throw new ProductException(INVALID_PRODUCT_CODE + productCode);
        } else {
            return product;
        }
    }

    private Product getProduct(String Product) {
        String[] values = Product.split(",");
        return new Product(values[0].trim(), values[1].trim());
    }

    private void loadProductMap() {
        try {
            productMap = parseProductList(FileUtils.loadTextFile(PRODUCT_FILE));
        } catch (RuntimeException rex) {
            System.out.println(rex.getMessage());
        }
    }

    public Map<String, Product> parseProductList(List<String> productList) {
        return productList.stream()
        .map(this::getProduct)
        .collect(toMap(Product::getCode, product -> product));
    }

    private void loadPriceInProductMap() {
        Map<String, List<ProductPrice>> productPriceMap = getProductPriceMap();
        populatePriceInProductMap(productPriceMap);
    }

    private Map<String, List<ProductPrice>> getProductPriceMap() {
        try {
            return parseProductPrice(FileUtils.loadTextFile(PRICE_FILE));
        } catch (RuntimeException rex) {
            System.out.println(rex.getMessage());
            return emptyMap();
        }
    }

    public Map<String, List<ProductPrice>> parseProductPrice(List<String> productPriceList) {
        return productPriceList.stream()
            .map(this::getProductQuantityPrice)
            .collect(groupingBy(ProductPrice::getProductCode));
    }

    private ProductPrice getProductQuantityPrice(String productPrice) {
        String[] values = productPrice.split(",");
        return new ProductPrice(values[0].trim(), parseInt(values[1].trim()), parseFloat(values[2].trim()));
    }

    private void populatePriceInProductMap(Map<String, List<ProductPrice>> productPriceMap) {
        productPriceMap.entrySet().forEach(this::populatePriceInProduct);
    }

    private void populatePriceInProduct(Map.Entry<String, List<ProductPrice>> productPriceEntry) {
        Product product = productMap.get(productPriceEntry.getKey());

        productPriceEntry.getValue().forEach(productPrice -> {
            product.addPackPrice(productPrice.getPackSize(), productPrice.getPrice());
            }
        );
    }
}

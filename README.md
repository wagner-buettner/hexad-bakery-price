# Hexad Bakery Price

## Introduction
The objective of this project is calculate prices for Hexad Bakery.

Providing its customers with better price for individual items or prepackaged items.

You will provide one file to get the better price of the Orders:
- A file, named ```orders.txt```, with customer orders

## System requirements

- I'm building this project with Java 8, Ubuntu 18.04 and Maven 3.X

## Building project

This is a Apache Maven project.
So, get into the project folder and ```mvn clean install```

## Code Coverage

To get the Code Coverage Report, just run: ```mvn verify```. A report will be created at target folder:

    target/site/jacoco/index.html

## About Input Files

####orders.txt 
must have one route per line

```
10 VS5
14 MB11
13 CF
```
Where 10 it's the amount and VS5 is the product code.


####output will be on the screen
will have one result per line, like the examples below:

```
- 10 VS5 $17.98 = 2 x 5 $8.99
- 14 MB11 $54.8 = 1 x 8 $24.95 + 3 x 2 $9.95
- 13 CF $25.85 = 2 x 5 $9.95 + 1 x 3 $5.95
```

## Executing project

- Get in ```target``` folder inside the project (after building it with maven)
- Copy files: ```orders.txt, product.csv and product_price.csv``` given in the project folder root into: ```target``` folder
- Edit routes and questions if you want
- Run jar file: ```java -jar price-0.1.jar```
